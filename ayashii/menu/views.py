# pylint: disable=C0111,E1101
from django.conf import settings
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import (
    Series,
    StudioSeries,
    GenreSeries,
    Link,
    SiteIcon
)

SHOWS_PER_ROW = 5


@login_required
def main(request):
    """The function for /menu, displays info in the main page"""
    if request.method == 'GET':
        series_rows = _build_series_rows()
        return render(request, 'menu/menu.html', {'series_rows': series_rows})


@login_required
def search(request):
    """A user searches from the menu and matching Series objects are returned

    Valid search criteria include:
        - Series title
        - Series status
        - Genre
        - Studio
    """


def _build_series_rows():
    """Compile all Series objects for usage in context of main menu

    This function returns a list of lists. Each internal list has at most
    five tuples. Each tuple has:
        - a Series object
        - a string of all Studio names related to the Series
        - a string of all Genre names related to the Series
        - a MyAnimeList URL for the Series
    """
    all_series = Series.objects.filter(visible=True).order_by('-id')
    # Ceiling division of # of Series objs. with # of shows/row
    num_rows = -((-1 * all_series.count()) // SHOWS_PER_ROW)
    series_rows = [list() for i in range(num_rows)]
    row_index = 0

    for index, series in enumerate(all_series):
        # Process series for display
        series = _modify_series(series)

        # Construct the tuple
        series_tuple = _build_series_tuple(series)

        # Append the tuple to the current list
        int_order = (index % SHOWS_PER_ROW) + 1
        series_rows[row_index].append(series_tuple)

        # Move to the next list in series_row after every 5 series
        if int_order % SHOWS_PER_ROW == 0:
            row_index += 1

    return series_rows


def _build_series_tuple(series):
    """Return a series tuple for display in the template

    (series, studio_str, genre_str, links)
    series:     The Series object
    studio_str: A string of all the studios for the series, comma separated
    genre_str:  A string of all the genres for the series, comma separated
    links:      A list of tuples (url, site, favicon)
                    url:     The URL for the series to the site info was
                             scraped from
                    site:    Name of the site the series was scraped from
                    favicon: The relative path of the favicon for the site
    """
    # Construct the tuple
    studios = StudioSeries.objects.filter(series=series)
    studio_str = _studios_to_str(studios)
    genres = GenreSeries.objects.filter(series=series)
    genre_str = _genres_to_str(genres)

    # Construct the list of links and reference to site favicons
    links = list()
    link_objects = Link.objects.filter(series=series)
    for link in link_objects:
        favicon = SiteIcon.objects.get(site=link.site).img
        links.append((link.url, link.site, favicon))

    return (series, studio_str, genre_str, links)


def _genres_to_str(genre_series):
    """Turn a QuerySet of GenreSeries objects to a string for display"""
    genre_str = ""
    for gs_obj in genre_series:
        genre_str += f"{gs_obj.genre.name}, "
    # Set to 'N/A' if no genres, else trim off ', ' part
    genre_str = "N/A" if not genre_str else genre_str[:-2]

    return genre_str


def _studios_to_str(studio_series):
    """Turn a QuerySet of StudioSeries objects to a string for display"""
    studio_str = ""
    for ss_obj in studio_series:
        studio_str += f"{ss_obj.studio.name}, "
    # Set to 'N/A' if no studios, else trim off ', ' part
    studio_str = "N/A" if not studio_str else studio_str[:-2]

    return studio_str


def _modify_series(series):
    """Modify the series by adding default values for empty fields before
    it is displayed"""
    if not series.poster_img:
        series.poster_img = settings.DEFAULT_POSTER
    if not series.preview_gif:
        series.preview_gif = settings.DEFAULT_GIF

    return series
