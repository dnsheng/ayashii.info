# pylint: disable=C0111,R0903
"""
Info such as the genre, studio, character, etc. should be results that
a user would use while searching on the menu page. Thus, the models
are defined here instead of on the series page.
"""

from django.db import models
from django.conf import settings


class NoStripCharField(models.CharField):
    """A CharField that doesn't strip whitespace at beginning/end"""

    def formfield(self, **kwargs):
        kwargs['strip'] = False
        return super(NoStripCharField, self).formfield(**kwargs)


class Series(models.Model):
    """Contains information regarding a show/movie/ova

    At a bare minimum, requires the title. May be instantiated by the
    file processor with images and gif paths set, or by the scraper with
    all but image and gif paths set.
    """
    title = models.CharField(max_length=200)
    header_one = NoStripCharField(max_length=150, blank=True)  # White text
    header_two = models.CharField(max_length=150, blank=True)  # Pink text
    description = models.TextField(blank=True)
    current_episodes = models.IntegerField(null=True, blank=True)
    total_episodes = models.IntegerField(blank=True)
    runtime = models.CharField(max_length=50, blank=True)
    show_format = models.CharField(max_length=50, blank=True)  # Movie, TV, OVA
    aired = models.CharField(max_length=50, blank=True)
    status = models.CharField(max_length=50, blank=True)  # Airing, Finished
    source = models.CharField(max_length=50, blank=True)  # Manga, 4Koma, etc.
    poster_img = models.ImageField(upload_to=settings.POSTER_PATH, blank=True)
    preview_img = models.ImageField(upload_to=settings.PREVIEW_IMG_PATH,
                                    blank=True)
    preview_gif = models.ImageField(upload_to=settings.PREVIEW_GIF_PATH,
                                    blank=True)
    rating = models.CharField(max_length=40, blank=True)  # R-17, PG13, etc.
    user_rating = models.DecimalField(max_digits=4,
                                      decimal_places=2,
                                      blank=True)
    visible = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Series"

    def __str__(self):
        return self.title


class Studio(models.Model):
    """Contains information regarding a studio"""
    name = models.CharField(max_length=100)
    url = models.CharField(max_length=300)
    img = models.ImageField(upload_to=settings.STUDIO_IMG_PATH, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Character(models.Model):
    """Contains information regarding a character"""
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    img = models.ImageField(upload_to=settings.CHAR_IMG_PATH, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Staff(models.Model):
    """Contains information regarding a person"""
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    given_name = models.CharField(max_length=50)
    family_name = models.CharField(max_length=50)
    img = models.ImageField(upload_to=settings.PERSON_IMG_PATH, blank=True)
    description = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = "Staff"

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Genre(models.Model):
    """Contains information regarding a genre"""
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.name


class SiteIcon(models.Model):
    """Links an anime site to a favicon file"""
    site = models.CharField(max_length=100, unique=True)
    img = models.ImageField(upload_to=settings.FAVICON_PATH)

    def __str__(self):
        return self.site


class Link(models.Model):
    """This maps a series to links for an anime info website"""
    site = models.CharField(max_length=100)
    url = models.CharField(max_length=200)
    series = models.ForeignKey('Series',
                               on_delete=models.CASCADE,
                               related_name='link')

    def __str__(self):
        return "{} -- {}".format(self.site, self.series)


class StudioSeries(models.Model):
    """This maps series to studios"""
    series = models.ForeignKey('Series', on_delete=models.CASCADE)
    studio = models.ForeignKey('Studio', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'StudioSeries'

    def __str__(self):
        return "{} -> {}".format(self.studio, self.series)


class StaffSeries(models.Model):
    """This maps staff to series"""
    series = models.ForeignKey('Series', on_delete=models.CASCADE)
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'StaffSeries'

    def __str__(self):
        return "{} -> {}".format(self.series, self.staff)


class CharacterStaffSeries(models.Model):
    """This maps characters to series"""
    series = models.ForeignKey('Series', on_delete=models.CASCADE)
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    character = models.ForeignKey('Character', on_delete=models.CASCADE)
    role = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = 'CharacterStaffSeries'

    def __str__(self):
        return "{} -> [{}] {} ({})".format(self.series, self.role,
                                           self.character, self.staff)


class GenreSeries(models.Model):
    """This maps genres to series"""
    series = models.ForeignKey('Series', on_delete=models.CASCADE)
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'GenreSeries'

    def __str__(self):
        return "{} -> {}".format(self.series, self.genre)
