# pylint: disable=E0611,E0401
"""This module holds the implementation for the 'scrape' command"""

from django.core.management.base import BaseCommand

from utils.scraping.scraper import Scraper
from utils.scraping.utils import build_headers


class Command(BaseCommand):
    """This command does the basic information scraping for any URL

    The 'scrape' command takes in one or more URLs with flags and it
    will scrape information from the URLs and store it in the database.
    The URLs are for pages that contain information on anime, characters,
    or staff. See the scraping tool (utils/scraping) for more details.

    After scraping the information, some fields may need further processing
    and so functions in the scraping utils are imported.
    """
    help = 'Scrapes information from a URL and loads it to the database'

    def add_arguments(self, parser):
        parser.add_argument('--visible',
                            action='store_true',
                            help='Make the series visible immediately '
                                 'after scraping completes')

        parser.add_argument('URL', nargs='+', type=str)

    def handle(self, *args, **options):
        scraper = Scraper()
        for url in options['URL']:
            # Check if series has already been scraped
            series = scraper.scrape_series(url, options)
            series.header_one, series.header_two = build_headers(series.title)
            series.save()
