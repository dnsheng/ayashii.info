from django.core.management.base import BaseCommand, CommandError

from menu.models import Series
# from utils.scraping import Scrape

class Command(BaseCommand):
    help = 'Something about moving the storage of website data \
            (DB, images, gifs, videos, etc.)'

    def add_arguments(self, parser):
        parser.add_argument('SRC_DIR', type=str)
        parser.add_argument('DST_DIR', type=str)

    def handle(self, *args, **options):
        # Move the contents from SRC_DIR to DST_DIR
        # Then edit all paths in DB
