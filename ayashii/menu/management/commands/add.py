from django.core.management.base import BaseCommand, CommandError

from menu.models import Series
# from utils.scraping import Scrape

class Command(BaseCommand):
    help = 'Add a new series to the site'

    def add_arguments(self, parser):
        parser.add_argument('URL', type=str)
        parser.add_argument('DIR', type=str)

    def handle(self, *args, **options):
        # Scrape the URL and save info to DB
        # Build screenshots and preview gifs from video files
        # Process the video files to some streaming format
