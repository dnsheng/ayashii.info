import sys
import argparse

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from utils.processing.processor import (
    build_gif,
    build_stream,
    build_timestamps,
    extract_subtitles,
    extract_fonts,
)

DEFAULT_GIF_FRAMES = 6
DEFAULT_START_TIME = 40


class Command(BaseCommand):
    help = 'Process a video file for the site'

    def add_arguments(self, parser):
        """Parse arguments for the 'process' command

        This command is broken into several subcommands:
            - image         Process the images and gifs from a video file
            - video         Process the video file into a stream format
            - subtitle      Extract the subtitle and fonts from a video file
        """
        parent_parser = argparse.ArgumentParser(add_help=False)
        parent_parser.add_argument('VIDEO',
                                   nargs='+',
                                   type=str,
                                   help='Video file(s) to process')
        subparsers = parser.add_subparsers(title='subcommands')

        # ====================================================================
        # Image parser
        # ====================================================================

        img_parser = subparsers.add_parser('image',
                                           parents=[parent_parser],
                                           help='Process images and gifs \
                                                 from video files')
        img_parser.add_argument('--start',
                                type=int,
                                default=DEFAULT_START_TIME,
                                help='Number of seconds in to start gif ('
                                     'default: {})'.format(DEFAULT_START_TIME))

        img_parser.add_argument('--end',
                                type=int,
                                default=-(sys.maxsize-1-DEFAULT_START_TIME),
                                help='Number of seconds in to end gif')

        img_parser.add_argument('--frames',
                                type=int,
                                default=DEFAULT_GIF_FRAMES,
                                help='Number of frames in the gif ('
                                     'default {})'.format(DEFAULT_GIF_FRAMES))

        img_parser.set_defaults(which='image')

        # ====================================================================
        # Video parser
        # ====================================================================

        vid_parser = subparsers.add_parser('video',
                                           parents=[parent_parser],
                                           help='Create streams')
        vid_parser.set_defaults(which='video')

        # ====================================================================
        # Subtitle parser
        # ====================================================================

        sub_parser = subparsers.add_parser('subtitle',
                                           parents=[parent_parser],
                                           help='Extract subs')
        sub_parser.set_defaults(which='subtitle')

    def handle(self, *args, **options):
        """Handle the subcommands and args called from the 'process' command"""
        # Image subcommand
        if options['which'] == 'image':
            print("You did the image subcommand")
            print(options['start'])
        # Video subcommand
        if options['which'] == 'video':
            print("You did the video subcommand")
        # Subtitle subcommand
        if options['which'] == 'subtitle':
            print("You did the subtitle subcommand")
            for video in options['VIDEO']:
                subs = extract_subtitles(video, "", "")
                print(subs)
                fonts = extract_fonts(video)
                print(fonts)


