# pylint: disable=E0401
"""This module holds the implementation for the 'init' command"""

import os

from django.conf import settings
from django.core.management.base import BaseCommand

from menu.models import SiteIcon


class Command(BaseCommand):
    """A command to initialize the site's backend on first install

    The init command does the following:
        - Creates the file directory structure for the static assets
        - Populates the database with initial information
    """
    help = ('Initializes the asset directory structure and populates '
            'the database')

    def handle(self, *args, **options):
        """Builds asset directory structure and populates the database"""
        self._build_folders()
        self._populate_db()

    @staticmethod
    def _build_folders():
        """Builds the asset directory file structure"""
        Command._check_dir_and_build(settings.AYASHII_STORAGE)
        Command._check_dir_and_build(settings.FONT_PATH)
        Command._check_dir_and_build(settings.SUBTITLE_PATH)
        Command._check_dir_and_build(settings.VIDEO_PATH)
        Command._check_dir_and_build(settings.PREVIEW_GIF_PATH)
        Command._check_dir_and_build(settings.PREVIEW_IMG_PATH)
        Command._check_dir_and_build(settings.USER_IMG_PATH)
        Command._check_dir_and_build(settings.POSTER_PATH)
        Command._check_dir_and_build(settings.STUDIO_IMG_PATH)
        Command._check_dir_and_build(settings.PERSON_IMG_PATH)
        Command._check_dir_and_build(settings.CHAR_IMG_PATH)

    @staticmethod
    def _check_dir_and_build(relative_path):
        """If the directory does not exist, then it is created"""
        path = os.path.join(settings.BASE_DIR, relative_path)
        if not os.path.exists(path):
            os.makedirs(path)

    @staticmethod
    def _populate_db():
        """Populates all the database tables"""
        Command._populate_siteicon()

    @staticmethod
    def _populate_siteicon():
        """Populate the SiteIcon database"""
        mal_favicon = os.path.join(settings.FAVICON_PATH, 'mal.ico')
        anidb_favicon = os.path.join(settings.FAVICON_PATH, 'anidb.png')

        # Ensure only one entry per site and avoid dealing with exceptions
        SiteIcon.objects.get_or_create(site="MyAnimeList", img=mal_favicon)
        SiteIcon.objects.get_or_create(site="AniDB", img=anidb_favicon)
