from django.core.management.base import BaseCommand, CommandError

from menu.models import Series

class Command(BaseCommand):
    help = 'Remove any abandoned image files in assets not linked to an object'

    def add_arguments(self, parser):

    def handle(self, *args, **options):
