from django.contrib import admin
from .models import (
    Series,
    Studio,
    Character,
    Staff,
    Genre,
    SiteIcon,
    Link,
    StudioSeries,
    StaffSeries,
    CharacterStaffSeries,
    GenreSeries,
)

# Register your models here.

admin.site.register(Series)
admin.site.register(Studio)
admin.site.register(Character)
admin.site.register(Staff)
admin.site.register(Genre)
admin.site.register(SiteIcon)
admin.site.register(Link)
admin.site.register(StudioSeries)
admin.site.register(StaffSeries)
admin.site.register(CharacterStaffSeries)
admin.site.register(GenreSeries)
