This is a series-specific page, with the main features being:
	+ the ability to watch episodes with subtitles
	+ view series information
	+ user-submitted reviews
	+ user ratings
	+ video comments
