from django.db import models
from django.contrib.auth.models import User

class SeriesEpisodes(models.Model):
	series = models.ForeignKey('Series', on_delete=models.CASCADE)
	episode_num = models.IntegerField()
	video = models.CharField(max_length=200)
	x_res = models.IntegerField()
	y_res = models.IntegerField()
	duration = models.DurationField()
	
	class Meta:
		verbose_name_plural = "Episode"

class SeriesSubs(models.Model):
	episode = models.ForeignKey('Episode', on_delete=models.CASCADE)
	subtitle = models.CharField(max_length=200)
	language = models.CharField(max_length=3)

# This maps user ratings to series
class SeriesRatings(models.Model):
    series = models.ForeignKey('Series', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

# This maps user ratings to characters
class CharacterRatings(models.Model):
    character= models.ForeignKey('Character', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

# This maps user ratings to staff
class StaffRating(models.Model):
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

# This maps user ratings to studios
class StudioRating(models.Model):
    studio = models.ForeignKey('Studio', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class SeriesComment(models.Model):
	series = models.ForeignKey('Series', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.TextField()
	timestamp = models.DateField(auto_now_add=True)

class CharacterComment(models.Model):
	character = models.ForeignKey('Character', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.TextField()
	timestamp = models.DateField(auto_now_add=True)

class StaffComment(models.Model):
	staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.TextField()
	timestamp = models.DateField(auto_now_add=True)

class StudioComment(models.Model):
	studio = models.ForeignKey('Studio', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.TextField()
	timestamp = models.DateField(auto_now_add=True)

