"""This module contains functions used to process video files"""

import re
import os
import glob
import uuid
import subprocess
from typing import Tuple, List, Union, Dict

from django.conf import settings


def build_timestamps(start: int, end: int, frames: int) -> List[int]:
    """Build a list of timestamps for a video given a start and end time

    The start and end timestamps are inclusive
    """
    interval = (end - start)/(frames-1)
    return [int(start + (interval * frame)) for frame in range(frames)]


def build_gif(video: str, dest: str, timestamps: List[int],
              resolution: Tuple[int, int], framerate: int) -> str:
    """Build a gif from a video and save it, returning the path

    Args:
        video           - The video file
        dest            - Directory to save the gif to
        frames          - A list of timestamps used to build a gif
        resolution      - A tuple for the final gif resolution (width, height)
    """
    base_name = os.path.join(settings.TEMP_DIR, uuid.uuid4().hex)
    for index, timestamp in enumerate(timestamps):
        image_path = f"{base_name}_{index}.jpg"
        extract_frame(video, image_path, timestamp, resolution)

    images = f"{base_name}_%d.jpg"
    gif_file = _compile_gif(images, dest, resolution, framerate)

    temp_images = f"{base_name}*.jpg"
    for image in glob.glob(temp_images):
        os.remove(image)

    return gif_file


def build_stream(video, dest, options) -> str:
    """Build a stream from a video file

    Options:
        - Format (HLS, m3u8, etc.)
    """


def extract_subtitles(video, dest, options) -> List[str]:
    """Extract the subtitles from a video file

    Returns a list of subtitle files extracted.
    """
    sub_dict = _build_sub_dict(video)
    for num, subtitle in sub_dict.items():
        sub_file = os.path.join(settings.SUBTITLE_PATH, subtitle)
        sub_arg = f"{num}:{sub_file}"
        extract_args = ['tracks', video, sub_arg]
        _call_command('mkvextract', extract_args)
    return sub_dict.values()


def _build_sub_dict(video) -> Dict[int, str]:
    """Return a dictionary of track id mapped to subtitle file"""
    info_args = ['-i', video]
    output = _call_command('mkvmerge', info_args).decode()
    sub_dict = dict()
    for line in output.split('\n'):
        if 'Track' in line and 'subtitles' in line:
            sub_name = f"{uuid.uuid4().hex}.ass"
            track_id = re.match(r'.*\s(\d):', line).group(1)
            sub_dict[track_id] = sub_name
    return sub_dict


def extract_fonts(video) -> List[str]:
    """Extract the fonts from the video

    Saves the fonts to settings.SUBTITLE_PATH if they don't already exist.
    Returns a list of all fonts from the video.
    """
    font_dict = _build_font_dict(video)
    for num, font in font_dict.items():
        font_file = os.path.join(settings.FONT_PATH, font)
        font_arg = f"{num}:{font_file}"
        extract_args = ['attachments', video, font_arg]
        _call_command('mkvextract', extract_args)
    return font_dict.values()


def _build_font_dict(video) -> Dict[int, str]:
    """Return the dict of attachment number mapped to font files for a video"""
    info_args = ['-i', video]
    output = _call_command('mkvmerge', info_args).decode()
    fonts = _parse_font_output(output)
    font_dict = {index+1: font for index, font in enumerate(fonts)}
    return font_dict


def _parse_font_output(output) -> List[str]:
    """Parse the output of a 'mkvmerge -i' command, returning fonts"""
    font_pattern = r".*\'([\w\s_-]+\.[to]tf).*"
    fonts = list()
    for line in output.split('\n'):
        search = re.search(font_pattern, line)
        if search:
            fonts.append(search.group(1))
    return fonts


def extract_frame(video: str, dest_path: str, time: int,
                  resolution: Tuple[int, int]) -> None:
    """Extract a single frame from a video file and save it

    Args:
        video       - The video file
        dest_path   - Path/filename to save image to
        time        - Timestamp to build image
        resolution  - A tuple (width, height)
    """
    image_args = ['-loglevel', 'quiet',
                  '-y',
                  '-ss', str(time),
                  '-i', str(video),
                  '-vframes', '1',
                  '-q:v', '2',
                  '-vf', f'scale={resolution[0]}x{resolution[1]}',
                  dest_path]
    _call_command('ffmpeg', image_args)


def _call_command(program: str, arg_list: List[Union[str, int]]):
    """Call a program with a list of arguments"""
    # Convert the arg_dict to a list for subprocess
    arg_list.insert(0, program)
    return subprocess.check_output(arg_list)


def _compile_gif(images: str, dest: str,
                 resolution: Tuple[int, int], framerate: int) -> str:
    """Render a gif out of images using ffmpeg

    Args:
        images      - Should be of some format '/path/to/imgs/img_%02d.jpg'
        dest        - Path to a folder
        resolution  - A tuple: (width, height)
        framerate   - An integer
    """
    # Create the output files (temporary and final)
    video_file = os.path.join(settings.TEMP_DIR,
                              f"{uuid.uuid4().hex}.mp4")
    palette_file = os.path.join(settings.TEMP_DIR,
                                f"{uuid.uuid4().hex}.png")
    gif_file = os.path.join(dest,
                            f"{uuid.uuid4().hex}.gif")

    # Build the list of args for the ffmpeg commands
    video_args = ['-loglevel', 'quiet',
                  '-y',
                  '-framerate', str(framerate),
                  '-i', images,
                  '-vf', f"scale={resolution[0]}x{resolution[1]}",
                  video_file]
    palette_args = ['-loglevel', 'quiet',
                    '-y',
                    '-i', video_file,
                    '-vf', 'palettegen', palette_file]
    gif_args = ['-loglevel', 'quiet',
                '-y',
                '-i', video_file,
                '-i', palette_file,
                '-lavfi', 'paletteuse', gif_file]

    # Execute the ffmpeg commands
    _call_command('ffmpeg', video_args)
    _call_command('ffmpeg', palette_args)
    _call_command('ffmpeg', gif_args)

    # Remove temp files (video and palette)
    os.remove(video_file)
    os.remove(palette_file)

    return gif_file
