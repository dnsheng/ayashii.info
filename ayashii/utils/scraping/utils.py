"""This module contains utility functions for scraping"""


def build_headers(title):
    """Return header_one and header_two from the title

    Multiple words:
        header_one = first 1/2 of words
        header_two = second 1/2 of words
    One word, short:
        header_one = title
        header_two = None
    One word, has '-':
        header_one = chars before '-'
        header_two = chars after '-'
    One word:
        header_one = first 1/2 of chars
        header_two = second 1/2 of chars
    """
    # Multiple words in title
    if ' ' in title:
        word_list = title.split(' ')
        half = int(len(word_list)/2)
        return (' '.join(word_list[:half]) + " ",
                ' '.join(word_list[half:]))
    # One short word title
    if len(title) < 7:
        return (title, None)
    # Dash in title
    if '-' in title:
        pre_dash, post_dash = title.split('-')
        return (f"{pre_dash}-", post_dash)
    # Just one word
    half = int(len(title)/2)
    return (title[:half], title[half:])
