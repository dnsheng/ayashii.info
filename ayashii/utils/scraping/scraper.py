"""This module contains the Scraper class"""
# pylint: disable=E0611,E0401

from multiprocessing.dummy import Pool as ThreadPool

from utils.scraping.extractors import get_extractors
from menu.models import (
    Series,
    Link,
)


class Scraper:
    """An interface that directly deals with the extractors and can return
    models from Django"""
    def __init__(self):
        self._extractors = get_extractors()
        self._pool = ThreadPool(5)

    def _get_extractor(self, url: str):
        """Returns the appropriate extractor based on the URL"""
        for extractor in self._extractors:
            if extractor.is_page(url):
                return extractor
        raise Exception(f"No valid extractor found for {url}")

    @staticmethod
    def _scrape_series_info(extractor, options) -> Series:
        """Scrape a URL for a series, returning a Series object

        Assuming that all goes well, Series is only missing: current_episodes,
        header_one, header_two, preview_img, and preview_gif.
        None of which is the responsibility of the scraper.
        """
        # Create a new Series object
        series = Series()

        # Apply series attributes
        series.title = extractor.series_title()
        series.description = extractor.series_synopsis()
        series.total_episodes = extractor.series_episodes()
        series.runtime = extractor.series_runtime()
        series.show_format = extractor.series_format()
        series.aired = extractor.series_aired()
        series.status = extractor.series_status()
        series.source = extractor.series_source()
        series.poster_img = extractor.series_poster()
        series.rating = extractor.series_rating()
        series.user_rating = extractor.series_score()

        # Set visibility
        if 'visible' in options:
            series.visible = options['visible']

        return series

    def scrape_series(self, url: str, options) -> Series:
        """Scrape all information for a new series

        This will scrape information for a new series object, as well as for
        any new studios, genres, characters, and staff objects.
        For new objects other than the series, a bare minimum of data is
        stored:
            Character: name, image
            Staff: name, image
            Char/Staff: role
            Genre: name
            Studio: name
        Everything scraped is automatically saved into the database since
        some models rely on others as foreign keys.

        The method will return the new Series object to allow the invoking
        function to apply fields such as 'preview_img'.
        """
        # Get the extractor for the website and initialize
        extractor = self._get_extractor(url)
        url = extractor.clean_url(url)

        # Check if series was previously scraped from the site
        if Link.objects.filter(site=extractor.name, url=url).exists():
            raise Exception("Series already scraped from the site")

        # Otherwise, initialize extractor
        extractor.init(url=url, series=True)

        # Create a new Series object
        series = self._scrape_series_info(extractor, options)
        series.save()

        # Create a new Link object
        link = Link(site=extractor.name, url=url, series=series)
        link.save()

        # Create the new Character objects

        # Create the new Staff objects

        # Create the new Studio objects

        # Create the new Genre objects
        genre_dict = extractor.series_genres()
        #print(genre_dict)

        # Link the Characters, Staff, and Series

        # Link the Series to Genres

        # Link the Series to Studios

        return series

    def scrape_character(self, url: str):
        """Scrape a URL for a character, returning a Character object
        """
