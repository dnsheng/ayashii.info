"""This module generates a list of all extractors"""

from .extractors import *

_ALL_CLASSES = [
    klass
    for name, klass in globals().items()
    if name.endswith('EX')
]


def _gen_extractor_classes():
    return _ALL_CLASSES


def get_extractors():
    """Return a list of all extractors"""
    return [klass() for klass in _gen_extractor_classes()]
