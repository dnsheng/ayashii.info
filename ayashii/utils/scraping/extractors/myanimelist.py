"""This module contains the MyAnimeList extractor"""
# pylint: disable=E0611,E0401

import os
import re
import uuid
from urllib.parse import quote

from django.conf import settings

from utils.scraping.extractors.extractor import Extractor


class MyAnimeListEX(Extractor):
    """Implements information extracting/scraping for MyAnimeList"""

    def __init__(self):
        super().__init__()
        self.name = "MyAnimeList"
        self._pattern = (r'https?://(?:www\.)?myanimelist\.net/anime/\d+/\w+'
                         r'[\w%\?=#]*')

        # Explicit declaration of internal attributes for clarity's sake
        self._soup = None
        self._tags = None
        self._relatives = None

    # =========================================================================
    # Overridden methods
    # =========================================================================

    def init(self, **kwargs):
        """Clean a URL and make a request to get the soup

        kwargs:
            url:    The URL for the website to scrape
            series: If the URL is for a series page (anime/movie/ova)

        If the URL is for a series, then also get the tags and relatives
        """
        # Get the soup for the url
        url = kwargs['url']
        self._soup = self._get_soup(url)

        if 'series' in kwargs:
            # Save the tags and relatives for easier extracting
            self._tags = self._soup.find_all("span", {"class": "dark_text"})
            relative_class = {"class": "ar fw-n borderClass"}
            self._relatives = self._soup.find_all("td", relative_class)

    def reset(self):
        """Set all internal attributes to None"""
        self._soup = None
        self._tags = None
        self._relatives = None

    @staticmethod
    def clean_url(url: str) -> str:
        """Clean unicode characters from URL so it is readable by urllib"""
        url = MyAnimeListEX._standardize_url(url)
        split = url.rsplit('/', 1)
        url_start, url_end = split[0], split[1]
        new_end = quote(url_end)
        return f"{url_start}/{new_end}"

    # =========================================================================
    # Implemented methods
    # =========================================================================

    def series_poster(self):
        """Download the poster image and returns the path it was saved to"""
        img_url = self._get_img_url(self._soup)
        img = self._download_img(self._session, img_url, settings.POSTER_PATH)
        return img

    def series_title(self):
        """Returns the title of the series"""
        return self._soup.find(itemprop="name").get_text()

    def series_synopsis(self):
        """Returns the synopsis of the series"""
        syn = self._soup.find(itemprop="description").get_text()
        return self._syn_format(syn)

    def series_format(self):
        """Returns the format of the series"""
        tag_index = self._index_for_tag(self._tags, "Type:")
        if tag_index:
            return self._tags[tag_index].next_sibling.next_sibling.string
        return None

    def series_episodes(self):
        """Returns the number of episodes in the series"""
        return self._search_index(self._tags, "Episodes:")

    def series_status(self):
        """Returns the status of the series"""
        return self._search_index(self._tags, "Status:")

    def series_aired(self):
        """Returns when the series was aired"""
        return self._search_index(self._tags, "Aired:")

    def series_premiered(self):
        """Returns when the series premiered"""
        return self._search_index(self._tags, "Premiered:")

    def series_broadcast(self):
        """Returns when the series is/was broadcasted"""
        return self._search_index(self._tags, "Broadcast:")

    def series_source(self):
        """Returns the source material type of the series"""
        return self._search_index(self._tags, "Source:")

    def series_runtime(self):
        """Returns the runtime/duration of the series"""
        return self._search_index(self._tags, "Duration:")

    def series_rating(self):
        """Returns the content rating of the series"""
        return self._search_index(self._tags, "Rating:")

    def series_score(self):
        """Returns the user score/rating of the series"""
        return self._soup.find(itemprop="ratingValue").get_text()

    def series_producers(self):
        """Returns a dict of MAL producer ID to producer name for the series"""
        return self._search_index_span(self._tags, "Producers:")

    def series_licensors(self):
        """Returns a dict of MAL licensor ID to licensor name for the series"""
        return self._search_index_span(self._tags, "Licensors:")

    def series_studios(self):
        """Returns a dict of MAL studio ID to studio name for the series"""
        return self._search_index_span(self._tags, "Studios:")

    def series_genres(self):
        """Returns a dict of MAL genre ID to genre name for the series"""
        return self._search_index_span(self._tags, "Genres:")

    # =========================================================================
    # Internal methods
    # =========================================================================

    @staticmethod
    def _standardize_url(url: str) -> str:
        """Remove any '#' chars or query substrings so the URL is clean"""
        url = url.split('?')[0]
        url = url.split('#')[0]
        return url

    @staticmethod
    def _get_img_url(soup) -> str:
        """Return the image URL from the soup"""
        return soup.find(itemprop="image")["src"]

    @staticmethod
    def _download_img(session, url: str, dest: str) -> str:
        """Download an image from the URL to the 'dest' folder, returning the
        full path of the newly downloaded image.

        If the image couldn't be downloaded, then an empty string is returned.
        The name of the newly downloaded image is a random hex UUID.
        """
        img_file = f"{uuid.uuid4().hex}.jpg"
        img_dest = os.path.join(dest, img_file)
        false_img = "https://myanimelist.cdn-dena.com/img/sp/icon/" \
                    "apple-touch-icon-256.png"
        if url != false_img:
            img = session.get(url, stream=True, verify=False)
            with open(img_dest, 'wb') as file_:
                for chunk in img.iter_content(chunk_size=1024):
                    file_.write(chunk)
            return img_dest
        return ""

    @staticmethod
    def _syn_format(synopsis: str) -> str:
        """Remove credits/source, showing only the relevant information"""
        synopsis = re.sub(r'\[Written by.*', '', synopsis)
        synopsis = re.sub(r'\[|\(Source.*', '', synopsis)
        return synopsis

    @staticmethod
    def _index_for_tag(tags, field):
        """Return the index for a field within the MAL tags"""
        for index, item in enumerate(tags):
            if item.string == field:
                return index
        return None

    @staticmethod
    def _tag_index_tostr(tag_index) -> str:
        """The string from a MAL tag_index (span) is returned"""
        return tag_index.next_sibling.strip()

    @staticmethod
    def _search_index(tags, query: str) -> str:
        tag_index = MyAnimeListEX._index_for_tag(tags, query)
        if tag_index:
            return MyAnimeListEX._tag_index_tostr(tags[tag_index])
        return None

    @staticmethod
    def _get_id(url: str) -> str:
        """Cut the ID from a MyAnimeList URL"""
        return url.rsplit('/', 2)[1]

    @staticmethod
    def _parse_span(span):
        """A dictionary of items for MAL is returned, where [ID] maps
        to "Name".

        It is used for: Producers, Licensors, Studios, and Genres
        """
        mal_dict = dict()
        for item in span.next_siblings:
            if item.name == "a":
                item_id = MyAnimeListEX._get_id(item.get('href'))
                item_name = item.string
                if item_name in ('None found', 'add some'):
                    mal_dict[item_id] = item_name
        return mal_dict

    @staticmethod
    def _search_index_span(tags, query: str) -> str:
        tag_index = MyAnimeListEX._index_for_tag(tags, query)
        if tag_index:
            return MyAnimeListEX._parse_span(tags[tag_index])
        return None
