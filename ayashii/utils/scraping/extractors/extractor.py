"""This module contains the abstract Extractor class"""
# pylint: disable=R0904,E1101

import re
import abc
from typing import Tuple

import requests
from PIL import Image
from bs4 import BeautifulSoup


class Extractor(abc.ABC):
    """An abstract class that handles the implementation details of website
    scraping/information extraction.
    """
    def __init__(self):
        self.name = ''
        self._pattern = r""
        self._session = requests.Session()
        self._session.headers = {"User-Agent": "ayashii/0.1",
                                 "Accept-encoding": "gzip"}
        requests.packages.urllib3.disable_warnings()

    # ========================================================================
    # Internal methods
    # ========================================================================

    def _get_soup(self, url: str):
        """For a URL, use the owned session to send a request and get the soup

        This is for internal use by the class that implements Extractor
        """
        request = self._session.get(url, verify=False)
        request.raise_for_status()
        content = request.content
        return BeautifulSoup(content, 'html.parser')

    # ========================================================================
    # Public methods
    # ========================================================================

    def init(self, **kwargs):
        """Given the URL, let the extractor set any internal
        states/variables"""

    def is_page(self, url: str) -> bool:
        """Check if URL passed is relevant to the extractor"""
        return bool(re.match(self._pattern, url))

    def reset(self):
        """May need to be called to reset current state of extractor"""

    @staticmethod
    def clean_url(url: str) -> str:
        """Clean the URL and standardize it.

        This is done because sometimes the URL may include queries and '#'.
        Cleaning it allows a unique lookup in Link() to see if there already
        exists a Series object for the series and site.
        """
        return url

    @staticmethod
    def resize_img(img: str, size: Tuple[int, int]) -> None:
        """Resize an image, overwriting the previous one"""
        image = Image.open(img)
        resize = image.resize(size)
        resize.save(img)

    # =======================================================================
    # Scraping methods
    # ========================================================================

    # ### Scraping for shows ###

    @abc.abstractmethod
    def series_poster(self):
        """Download the poster image and return the file's path"""

    @abc.abstractmethod
    def series_title(self):
        """Return the series title"""

    @abc.abstractmethod
    def series_synopsis(self):
        """Return the synopsis of the series"""

    @abc.abstractmethod
    def series_format(self):
        """Return the format of the series (TV, Movie, OVA, etc.)"""

    @abc.abstractmethod
    def series_episodes(self):
        """Return the # of episodes in the series"""

    @abc.abstractmethod
    def series_status(self):
        """Return the series status (Finished, Currently airing, etc.)"""

    @abc.abstractmethod
    def series_aired(self):
        """Return when the series airs/aired"""

    @abc.abstractmethod
    def series_premiered(self):
        """Return when the series premiers/premiered"""

    @abc.abstractmethod
    def series_broadcast(self):
        """Return when the series broadcasts/was broadcasted"""

    @abc.abstractmethod
    def series_source(self):
        """Return the source of the series (Novel, Original, etc.)"""

    @abc.abstractmethod
    def series_producers(self):
        """Return a dictionary of producers, [ID] mapped to name"""

    @abc.abstractmethod
    def series_licensors(self):
        """Return a dictionary of licensors, [ID] mapped to name"""

    @abc.abstractmethod
    def series_studios(self):
        """Return a dictionary of studios, [ID] mapped to name"""

    @abc.abstractmethod
    def series_genres(self):
        """Return a dictionary of genres, [ID] mapped to name"""

    @abc.abstractmethod
    def series_runtime(self):
        """Return the runtime duration of the series"""

    @abc.abstractmethod
    def series_rating(self):
        """Return the rating/certification of the series (PG, R, etc.)"""

    @abc.abstractmethod
    def series_score(self):
        """Return the viewer/critic score of the series"""

    # ### Scraping for characters ###

    # @abc.abstractmethod
    # def char_id(self):
    #     """Return the character's id"""

    # @abc.abstractmethod
    # def char_image(self):
    #     """Download the character's image and return the file path"""

    # @abc.abstractmethod
    # def char_name(self):
    #     """Return the character's name"""

    # @abc.abstractmethod
    # def char_desc(self):
    #     """Return the character's description"""

    # ### Scraping for people ###

    # @abc.abstractmethod
    # def person_id(self):
    #     """Return the person's id"""

    # @abc.abstractmethod
    # def person_image(self):
    #     """Download the person's image and return the file path"""

    # @abc.abstractmethod
    # def person_desc(self):
    #     """Return the person's description/biography"""

    # ### Scraping for all ###

    # @abc.abstractmethod
    # def get_cast_list(self):
    #     """Return a list of roles and staff for the series:
    #         1. Roles - A list of Role dicts
    #         2. Staff - A list of Staff dicts
    #     """
