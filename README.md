![alt_text](docs/images/ayashii_logo.png)


## Description:

ayashii.info is an anime video streaming website featuring user profiles, comments, ratings, 
and information regarding shows, characters, staff, and studios. The information is gathered
using backend utilities that scrape from common/popular sites.


Currently it is in an extremely early developmental stage and the application scope is not
clearly defined.

## Requirements:

#### Python:

+ python 3.7+
+ django
+ requests
+ BeautifulSoup4
+ pillow

#### System:

+ ffmpeg
+ mkvextract (included in mkvtoolnix)


## Installation:

Start by installing all python library dependencies required:

```
pip install django requests bs4 pillow
```

Also ensure that the system dependencies are installed and are in your system's $PATH.

The system dependencies should be relatively up-to-date.

Then, create the database and asset file structure with Django's manager:

```
python manage.py makemigrations menu
python manage.py migrate
python manage.py createsuperuser
python manage.py init
```

Once initialized, the server can be run.

```
python manage.py runserver
```


## Usage:

Scrape information for a series by running the 'series' command:

```
python manage.py series https://myanimelist.net/anime/523/Tonari_no_Totoro
```


## Authors:

+ **DNSheng** [GitLab](https://gitlab.com/dnsheng)
